package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"time"
)

func main() {
	MakeRequest()
}

// MakeRequest - function makes a call to ES
// It submits message as json and shows the result of submission.
func MakeRequest() {
	t := time.Now()

	message := map[string]interface{}{
		"name": "John Smith",
		"age":  42,
		"emails": []string{
			"johns@email", "smithj@mbox",
		},
		"@timestamp": t.Local(),
	}

	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}

	//fmt.Println(bytesRepresentation)
	//fmt.Println(bytes.NewBuffer(bytesRepresentation))

	resp, err := http.Post("https://es.host.funtoo.org:9200/test/report", "application/json", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		log.Fatalln(err)
	}

	var result map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&result)

	log.Println(result)
	log.Println(result["data"])
}
